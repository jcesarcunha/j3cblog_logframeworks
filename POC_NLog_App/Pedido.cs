﻿using NLog;

namespace POC_NLog_App
{
    public class Pedido
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public int IdPedido { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }

        public void Salvar()
        {
            logger.Info("Salvando o pedido 1234");
            logger.Trace("Detalhes do pedido a ser salvo: " + this.ToString());
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}) - R$ {2}", Descricao, IdPedido, Valor.ToString("N2"));
        }
    }
}