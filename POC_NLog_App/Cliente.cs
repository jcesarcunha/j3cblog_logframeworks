﻿using System;
using NLog;

namespace POC_NLog_App
{
    public class Cliente
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void Salvar()
        {
            try
            {
                logger.Debug("Cliente: 123 | Julio");
                logger.Info("Tentando salvar o cliente XYZ...");
                throw new Exception("Dados inválidos");
            }
            catch (Exception ex)
            {
                logger.ErrorException("Erro ao salvar cliente", ex);
            }
        }
    }
}