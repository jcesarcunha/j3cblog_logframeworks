﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace POC_LogAppBlock_App
{
    public class Cliente
    {
        public void Salvar()
        {
            try
            {
                using (new Tracer("Salvando novo cliente"))
                {
                    Logger.Write("Cliente: 123 | Julio", "General");
                    Logger.Write("Tentando salvar o cliente XYZ...", "General");
                }
                throw new Exception("Dados inválidos");
            }
            catch (Exception ex)
            {
                Logger.Write("Erro ao salvar cliente", "Excecao");
            }
        }
    }
}