﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace POC_LogAppBlock_App
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Logger.IsLoggingEnabled())
            {
                Logger.Write("Iniciando a aplicação", "General", 1, 1, System.Diagnostics.TraceEventType.Start);
                Logger.Write("Debugando a aplicação", "General", 2, 1, System.Diagnostics.TraceEventType.Information);
                Logger.Write("Erro!", "General", 3, 1, System.Diagnostics.TraceEventType.Error);
                Logger.Write("Erro fatal... saindo da app", "General");
                Logger.Write("Opa... alerta!", "General");
            }

            Cliente c = new Cliente();
            c.Salvar();

            Pedido p = new Pedido();
            p.IdPedido = 123;
            p.Descricao = "Materiais de escritório";
            p.Valor = 89.7m;
            p.Salvar();

            Console.ReadKey();
        }
    }
}