﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace POC_LogAppBlock_App
{
    public class Pedido
    {
        public int IdPedido { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }

        public void Salvar()
        {
            Logger.Write("Salvando o pedido 1234", "General");
            Logger.Write("Detalhes do pedido a ser salvo: " + this.ToString(), "General");
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}) - R$ {2}", Descricao, IdPedido, Valor.ToString("N2"));
        }
    }
}