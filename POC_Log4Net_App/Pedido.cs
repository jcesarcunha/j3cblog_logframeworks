﻿using log4net;

namespace POC_Log4Net_App
{
    public class Pedido
    {
        private static ILog logger = LogManager.GetLogger(typeof(Pedido));

        public int IdPedido { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }

        public void Salvar()
        {
            logger.Info("Salvando o pedido 1234");
            logger.Debug("Detalhes do pedido a ser salvo: " + this.ToString());
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}) - R$ {2}", Descricao, IdPedido, Valor.ToString("N2"));
        }
    }
}