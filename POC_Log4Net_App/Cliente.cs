﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Config;

namespace POC_Log4Net_App
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public string Nome { get; set; }

        public void Salvar()
        {
            ILog loggerCliente = LogManager.GetLogger(typeof(Cliente));

            try
            {
                loggerCliente.Debug("Cliente: 123 | Julio");
                loggerCliente.Info("Tentando salvar o cliente XYZ...");
                throw new Exception("Dados inválidos");
            }
            catch (Exception ex)
            {
                loggerCliente.Error("Erro ao salvar cliente", ex);
            }
        }
    }
}