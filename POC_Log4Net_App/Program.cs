﻿using System;
using log4net;
using log4net.Config;

namespace POC_Log4Net_App
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            ILog logger = LogManager.GetLogger("POC_Log4Net_App");

            if (logger.IsInfoEnabled)
                logger.Info("Iniciando a aplicação");
            
            if (logger.IsDebugEnabled)
                logger.Debug("Debugando a aplicação");
            
            if (logger.IsErrorEnabled)
                logger.Error("Erro!", new Exception("detalhe do erro"));

            if (logger.IsFatalEnabled)
                logger.Fatal("Erro fatal... saindo da app");

            if (logger.IsWarnEnabled)
                logger.Warn("Opa... alerta!");

            Cliente c = new Cliente();
            c.Salvar();

            Pedido p = new Pedido();
            p.IdPedido = 123;
            p.Descricao = "Materiais de escritório";
            p.Valor = 89.7m;
            p.Salvar();

            Console.ReadKey();
        }
    }
}