﻿using System.Diagnostics;

namespace POC_SysDiag_App
{
    public class Pedido
    {
        public int IdPedido { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }

        public void Salvar()
        {
            Trace.TraceInformation("Salvando o pedido 1234");
            Trace.TraceInformation("Detalhes do pedido a ser salvo: " + this.ToString());
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}) - R$ {2}", Descricao, IdPedido, Valor.ToString("N2"));
        }
    }
}