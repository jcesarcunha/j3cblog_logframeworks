﻿using System;
using System.Diagnostics;

namespace POC_SysDiag_App
{
    public class Cliente
    {
        public void Salvar()
        {
            try
            {
                Debug.Write("Cliente: 123 | Julio");
                Trace.TraceInformation("Tentando salvar o cliente XYZ...");
                throw new Exception("Dados inválidos");
            }
            catch (Exception ex)
            {
                Trace.TraceError("Erro ao salvar cliente", ex);
            }
        }
    }
}