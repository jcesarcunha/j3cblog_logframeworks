﻿using System;
using System.Diagnostics;

namespace POC_SysDiag_App
{
    class Program
    {
        static void Main(string[] args)
        {
            Trace.TraceInformation("Iniciando a aplicação");
            Debug.Write("Debugando a aplicação");
            Trace.TraceError("Erro!", new Exception("Detalhe do erro"));
            Trace.TraceError("Erro fatal... saindo da app");
            Trace.TraceWarning("Opa... alerta!");

            Cliente c = new Cliente();
            c.Salvar();

            Pedido p = new Pedido();
            p.IdPedido = 123;
            p.Descricao = "Materiais de escritório";
            p.Valor = 89.7m;
            p.Salvar();

            Console.ReadKey();
        }
    }
}